-- config

-- NOTE: right now values 1 - 9 are supported.
-- Implement more buttons for more.
local BULB_COUNT = 5

local BLINK_COUNT = 5
local TEXT_SIZE = 20

-- utils

local rng = love.math.newRandomGenerator(love.timer.getTime())

local function class(statics)
	local t = statics or {}
	t.__index = t
	return t
end

local function get_random_numbers(maxv, count)
	local t = {}
	for i = 1, count do
		t[i] = rng:random(maxv)
	end
	return t
end

-- Simon

local Simon = class({hit = 1, win = 2, mishit = 3})

function Simon:new(bulb_count, blink_count)
	local t = {
		bulb_count = bulb_count,
		blink_count = blink_count,
		blinks = get_random_numbers(bulb_count, blink_count),
		guesses = {},
	}
	return setmetatable(t, Simon)
end

function Simon:guess(i)
	local guesses = self.guesses
	if i then
		table.insert(guesses, i)
	end
	if self.blinks[#guesses] == guesses[#guesses] then
		if #guesses >= self.blink_count then
			return Simon.win
		else
			return Simon.hit
		end
	end
	return Simon.mishit
end

-- Simon draw

local Draw = class({
	font = love.graphics.newFont(TEXT_SIZE),
	bulb_line_width = 15,
	bulb_off_color = {1, 1, 0},
	bulb_hit_color = {0, 1, 0},
	bulb_mis_color = {1, 0, 0},
	bulb_show_time = 1,
	fps = 1 / 60,
	congrats = "Congrats!",
	welp = "Welp",
})

-- Simon draw - drawing functions

function Draw.message(ctx, i)
	local j = ctx.jobs[i]
	love.graphics.print(j.label, j.x, j.y)
end

function Draw.bulb(ctx, i)
	local j = ctx.jobs[i]
	if ctx.next_frame_moment < j.last_hit + Draw.bulb_show_time then
		love.graphics.setColor(unpack(Draw.bulb_hit_color))
	else
		love.graphics.setColor(unpack(Draw.bulb_off_color))
	end
	love.graphics.circle("line", j.x, j.y, j.r)
	love.graphics.print(j.label, j.x, j.y)
end

function Draw.bulb_show(ctx, i)
	local j = ctx.jobs[i]
	love.graphics.setColor(unpack(Draw.bulb_off_color))
	local t = ctx.next_frame_moment - ctx.show_start
	local r = j.r - j.Dr * (t - Draw.bulb_show_time) * t
	love.graphics.circle("line", j.x, j.y, r)
	love.graphics.print(j.label, j.x, j.y)
end

function Draw.bulb_wrong(ctx, i)
	local j = ctx.jobs[i]
	love.graphics.setColor(unpack(Draw.bulb_mis_color))
	love.graphics.circle("line", j.x, j.y, j.r)
	love.graphics.print(j.label, j.x, j.y)
end

function Draw.nothing()
end

-- Simon draw - objects

function Draw:new(simon, w, h)
	local time = love.timer.getTime()
	local t = {
		simon = simon,
		jobs = {},
		current_bulb = 1, -- used in showing phase
		show_start = time,
		show_end = time + Draw.bulb_show_time,
		next_frame_moment = time,
		--viewport_w
		--viewport_h
		--viewport_margin
	}
	for i = 1, simon.bulb_count do
		table.insert(t.jobs, {last_hit = 0, drawer = Draw.bulb})
	end
	t.jobs[t.simon.blinks[t.current_bulb]].drawer = Draw.bulb_show
	table.insert(t.jobs, {drawer = Draw.nothing})
	setmetatable(t, Draw)
	t:resize(w, h)
	return t
end

function Draw:__call()
	for i = 1, #self.jobs do
		self.jobs[i].drawer (self, i)
	end
end

function Draw:set_text(i, t)
	local msg = self.jobs[i]
	if t then
		text = t
		msg.x = (self.viewport_w - Draw.font:getWidth(t)) * 0.50
		msg.y = self.viewport_h * 0.75
		msg.label = t
		msg.drawer = Draw.message
	else
		msg.drawer = Draw.nothing
	end
end

function Draw:set_bulb(i)
	local bulb = self.jobs[i]
	bulb.r = self.viewport_margin * 4
	bulb.Dr = 2 * bulb.r
	bulb.x = (2 * i - 1) * (bulb.r + self.viewport_margin)
	bulb.y = self.viewport_h / 2
	bulb.label = string.format("%d", i)
end

function Draw:resize(w, h, text)
	self.viewport_w = w
	self.viewport_h = h
	if w > h then
		self.viewport_margin = w / (10 * self.simon.bulb_count)
	else
		self.viewport_margin = h / (10 * self.simon.bulb_count)
	end
	for i = 1, self.simon.bulb_count do
		self:set_bulb(i)
	end
	self:set_text(#self.jobs, text)
end

function Draw:set_hit(i)
	self.jobs[i].last_hit = self.next_frame_moment
end

function Draw:set_win()
	local text = string.format(
		"%s\n%s", Draw.congrats, table.concat(self.simon.blinks, " "))
	self:set_text(self.simon.bulb_count + 1, text)
end

function Draw:set_mishit(i)
	local text = string.format(
		"%s\n%s\n%s",
		Draw.welp,
		table.concat(self.simon.blinks, " "),
		table.concat(self.simon.guesses, " "))
	self.jobs[i].drawer = Draw.bulb_wrong
	self:set_text(self.simon.bulb_count + 1, text)
end

function Draw:show_next()
	self.jobs[self.simon.blinks[self.current_bulb]].drawer = Draw.bulb
	self.current_bulb = self.current_bulb + 1
	if self.current_bulb > self.simon.bulb_count then
		return false
	end
	self.jobs[self.simon.blinks[self.current_bulb]].drawer = Draw.bulb_show
	self.show_start = self.show_end
	self.show_end = self.show_end + Draw.bulb_show_time
	return true
end

-- sound

local sound = {
	noice  = love.audio.newSource("noice.wav", "static"),
	showoff = love.audio.newSource("showoff.wav", "static"),
	whoops = love.audio.newSource("whoops.wav", "static"),
	congrats = love.audio.newSource("congrats.wav", "static"),
}

-- game

local game = {}

function game.restart()
	local w, h, _ = love.window.getMode()
	game.simon = Simon:new(BULB_COUNT, BLINK_COUNT)
	game.draw = Draw:new(game.simon, w, h)
	game.disable_guessing()
	love.update = game.step
end

local key_table = {
	q = love.event.quit,
	r = game.restart,
}

local action_table = {
	[Simon.hit] = function(i)
		game.draw:set_hit(i)
		sound.noice:stop()
		sound.noice:play()
		print("guessed!")
	end,
	[Simon.win] = function(i)
		game.draw:set_hit(i)
		game.draw:set_win()
		sound.congrats:play()
		game.disable_guessing()
	end,
	[Simon.mishit] = function(i)
		game.draw:set_mishit(i)
		sound.whoops:play()
		game.disable_guessing()
		print("moron...")
	end,
}

function game.enable_guessing()
	for i = 1, game.simon.bulb_count do
		key_table[tostring(i)] = function()
			action_table [game.simon:guess(i)] (i)
		end
	end
end

function game.disable_guessing()
	for i = 1, 9 do
		key_table[tostring(i)] = nil
	end
end

function game.next_frame(dt)
	game.draw.next_frame_moment = game.draw.next_frame_moment + Draw.fps
end

function game.step(dt)
	game.next_frame(dt)
	if game.draw.next_frame_moment >= game.draw.show_end then
		if not game.draw:show_next() then
			love.update = game.next_frame
			game.enable_guessing()
		end
		sound.showoff:stop()
		sound.showoff:play()
	end
end

-- glue code

function love.load()
	love.graphics.setLineWidth(Draw.bulb_line_width)
	love.graphics.setFont(Draw.font)
	game.restart()
end

function love.draw()
	game.draw()
	local cur_time = love.timer.getTime()
	if game.draw.next_frame_moment <= cur_time then
		game.draw.next_frame_moment = cur_time
		return
	end
	love.timer.sleep(game.draw.next_frame_moment - cur_time)
end

function love.resize(w, h)
	game.draw:resize(w, h)
end

function love.keyreleased(key, scancode)
	if key_table[key] then
		key_table[key] ()
	end
end

