-- config V

local simon = {
	-- NOTE: currently buttons 1-9 only are supported, so this shouldn't be >9
	bulb_count = 5,
	blink_count = 5,
	blinks = {},
}

local draw = {
	bulb = {
		segments = 100,
		line_width = 15,
		off_color = {1, 1, 0},
		hit_color = {0, 1, 0},
		mis_color = {1, 0, 0},
		show_time = 1,
	},
	viewport = {},
	fps = 1 / 60,
	congrats = {
		c = "Congrats!",
		w = "Welp",
		size = 20,
	},
}

-- config ^

local sound = {
	noice  = love.audio.newSource("noice.wav", "static"),
	showoff = love.audio.newSource("showoff.wav", "static"),
	whoops = love.audio.newSource("whoops.wav", "static"),
	congrats = love.audio.newSource("congrats.wav", "static"),
}

local phase -- 1 - showing blink order, 2 - playing, 3 - won, 4 - lost

local rng = love.math.newRandomGenerator(love.timer.getTime())

local font = love.graphics.getFont()

function reset_congrats()
	if draw.congrats.t ~= nil then
		draw.congrats.x = (draw.viewport.w - font:getWidth(draw.congrats.t)) * 0.50
		draw.congrats.y = draw.viewport.h * 0.75
	end
end

function reset_pos()
	local x, y
	for i = 1, simon.bulb_count, 1 do
		x = (2 * i - 1) * (draw.bulb.r + draw.viewport.margin)
		y = draw.viewport.half_h
		draw.bulb[i] = {x = x, y = y, label = string.format("%d", i)}
	end
end

function restart()
	for i = 1, simon.blink_count do
		simon.blinks[i] = rng:random(simon.bulb_count)
	end
	simon.guessed = 0
	phase = 1
	for i = 1, simon.bulb_count do
		draw.bulb[i].last_hit = nil
		draw.bulb[i].ishit = nil -- this variable name is epic
		                         -- is this 'is_hit or i_shit?
	end
	draw.bulb.current = 1
	draw.start = love.timer.getTime()
	draw.end_ = draw.start + draw.bulb.show_time
	draw.next_frame_time = draw.start
end

function love.load()
	local ww, wh, wm = love.window.getMode()
	love.resize(ww, wh)
	love.graphics.setLineWidth(draw.bulb.line_width)
	love.graphics.setFont(love.graphics.newFont(draw.congrats.size))
	restart()
end

function love.update(dt)
	draw.next_frame_time = draw.next_frame_time + draw.fps
	if phase == 1 then
		if love.timer.getTime() >= draw.end_ then
			draw.bulb.current = draw.bulb.current + 1
			draw.start = draw.end_
			draw.end_ = draw.end_ + draw.bulb.show_time
			sound.showoff:stop()
			sound.showoff:play()
			if draw.bulb.current > simon.blink_count then
				phase = 2
			end
		end
	end
end

function love.draw()
	for i = 1, simon.bulb_count, 1 do
		love.graphics.setColor(
				unpack(draw.bulb.off_color))
		local b, r, t
		b = draw.bulb[i]
		r = draw.bulb.r
		t = love.timer.getTime()
		if phase == 1 and i == simon.blinks[draw.bulb.current] then
			t = t - draw.start
			r = r - draw.bulb.Dr * (t - draw.bulb.show_time) * t
		else if phase == 2 and b.last_hit ~= nil and
				t < b.last_hit + draw.bulb.show_time then
			love.graphics.setColor(
					unpack(draw.bulb.hit_color))
		else if phase == 3 and b.ishit then
			love.graphics.setColor(
					unpack(draw.bulb.mis_color))
		else if phase == 3 and i == simon.blinks[simon.guessed] then
			love.graphics.setColor(
					unpack(draw.bulb.hit_color))
		end end end end
		love.graphics.circle("line", b.x, b.y, r, draw.bulb.segments)
		love.graphics.print(b.label, b.x, b.y)
	end
	if phase == 3 or phase == 4 then
		love.graphics.print(draw.congrats.t, draw.congrats.x, draw.congrats.y)
	end
	local cur_time = love.timer.getTime()
	if draw.next_frame_time <= cur_time then
		draw.next_frame_time = cur_time
		return
	end
	love.timer.sleep(draw.next_frame_time - cur_time)
end

function love.resize(w, h)
	draw.viewport.w = w
	draw.viewport.h = h
	draw.viewport.half_h = h / 2
	if w > h then
		draw.viewport.margin = w / (10 * simon.bulb_count)
	else
		draw.viewport.margin = h / (10 * simon.bulb_count)
	end
	draw.bulb.r  = draw.viewport.margin * 4
	draw.bulb.Dr = 2 * draw.bulb.r -- delta r
	reset_congrats()
	reset_pos()
end

function love.keyreleased(key, scancode)
	if key == "q" then
		love.event.quit()
	else if key == "r" then
		restart()
	else
		if phase == 2 then
			local guess = tonumber(key)
			if guess ~= nil then
				simon.guessed = simon.guessed + 1
				if guess == simon.blinks[simon.guessed] then
					draw.bulb[guess].last_hit = love.timer.getTime()
					print("guessed!")
					if simon.guessed >= simon.blink_count then
						phase = 4
						sound.congrats:play()
						draw.congrats.t = string.format("%s\n%s",
							draw.congrats.c, table.concat(simon.blinks, " "))
						reset_congrats()
					end
					sound.noice:stop()
					sound.noice:play()
				else
					phase = 3
					draw.bulb[guess].ishit = 1
					draw.congrats.t = string.format("%s\n%s", draw.congrats.w,
						table.concat(simon.blinks, " "))
					reset_congrats()
					print("moron...")
					sound.whoops:play()
				end
			end
		end
	end end
end

